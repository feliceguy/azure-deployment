/*******************     Mekanate Bot with LUIS        **************************/

require('./connector.js')();
require('./dialogs/careers.js')();
require('./dialogs/botdetails.js')();
require('./dialogs/executives.js')();
require('./dialogs/general.js')();
require('./dialogs/industries.js')();
require('./dialogs/platforms.js')();
require('./dialogs/productservice.js')();
require('./dialogs/rating.js')();
require('./dialogs/resources.js')();
require('./dialogs/salesmarketing.js')();
require('./dialogs/technologies.js')();
