class Links {
	getLink(parameter) {
		var responses = {
			'CEO': 'https://www.youtube.com/watch?v=leOYqN4biGs&t=10s',
			'CPO': 'https://www.youtube.com/watch?v=P5rNCM8krDQ',
			'Digital Transformation': 'https://www.youtube.com/watch?v=KFFHb6wzdyU',
			'Suyati.About': 'https://youtu.be/2C-iVpb21GE',
			'Jobs': 'https://www.youtube.com/watch?v=SmIQ9YGVg5s',
		};
		return responses[parameter];
	}

}
module.exports = Links;