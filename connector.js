var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var Configuration = require('./config');
var Functions = require('./functions/general');
var Constants = require('./functions/constants');
var ChatHistory = require('./functions/chathistory');
var Store = require('./lib/store');

module.exports = function () {
    var restify = require('restify');
    global.builder = require('botbuilder');

    // Chat connector
    var connector = new builder.ChatConnector({
        appId: process.env.MicrosoftAppId,
        appPassword: process.env.MicrosoftAppPassword,
        openIdMetadata: process.env.BotOpenIdMetadata,
        gzipData: true
    });
    var inMemoryStorage = new builder.MemoryBotStorage();
     
    // #region 'Universal Bot, LUIS, recognizer'
    global.bot = new builder.UniversalBot(connector,(session, args) => {
        Functions.prototype.saveTranscript(session.message.text, session.message.timestamp);
        Constants.prototype.response = Store.prototype.getNoneResponse();
        var randomItem = Constants.prototype.response[Math.floor(Math.random() * Constants.prototype.response.length)];
        Functions.prototype.sendMessage([randomItem], session, { intent: { intent: 'None' } });
        Functions.prototype.saveTranscript('Bot', randomItem, Functions.prototype.getDateTime());
    }).set('storage', inMemoryStorage);
       


    //#region 'Restify Server, Connector, Listener'
    var server = restify.createServer();
    server.listen(process.env.port || process.env.PORT || 3978, function (args) {

        console.log(' Connected to Server : %s', server.name, server.url);
    });

    server.post('/api/messages', connector.listen());
    bot.use(builder.Middleware.dialogVersion({ version: 0.2, resetCommand: /^reset/i }));
      
    //   Listen for messages from users 
    server.post('/api/messages', [
        function (request, res, next) {
            ChatHistory.prototype.IPaddress = request.headers['x-forwarded-for'].split(',').pop() ||
            request.connection.remoteAddress ||
            request.socket.remoteAddress ||
            request.connection.socket.remoteAddress;

            Constants.prototype.timer = new Date(res._time);
            Constants.prototype.timerId = setInterval(function () {
                Constants.prototype.resetTimer = moment(new Date()).diff(moment(Constants.prototype.timer), 'minutes');
                if (Constants.prototype.resetTimer == 1 && (Constants.prototype.flag == false)) {
                    bot.beginDialog(msgAddress, 'Ideal');
                    Constants.prototype.flag = true;
                }
                else if (Constants.prototype.resetTimer == 3 && (Constants.prototype.flag == true)) {
                    ChatHistory.prototype.IdleTime = Constants.prototype.resetTimer + " Minutes";
                    Functions.prototype.savetoDB();
                    Constants.prototype.prototype.flag = false;
                    clearInterval(Constants.prototype.timerId);
                }
            }, 120000);
            next();
        },
        connector.listen()
    ]);

    clearInterval(Constants.prototype.timerId);
    //    #endregion

    bot.use(builder.Middleware.dialogVersion({ version: 0.2, resetCommand: /^reset/i }));
 
    //  LUIS URL
    var LuisModelUrl = 'https://' + Configuration.prototype.getLuisDetails('apiHost') + '/luis/v2.0/apps/' +
        Configuration.prototype.getLuisDetails('appId') + '?subscription-key=' + Configuration.prototype.getLuisDetails('apiKey');
     
    // Recognizer that gets intents from LUIS, and add it to the bot
    var recognizer = new builder.LuisRecognizer(LuisModelUrl).onEnabled((context, callback) => {
        var stack = context.dialogStack();
        var enabled = stack.length === 0;
        callback(null, enabled);
    });

    bot.recognizer(recognizer);
    //#endregion

    //#region 'Bot on - Conversation Update'
    bot.on('conversationUpdate', function (message) {        
        if (message.membersAdded) {
            message.membersAdded.forEach(function (identity) {
                if (identity.id == message.address.bot.id) {
                    Functions.prototype.initialise();
                    msgAddress = message.address;
                    ChatHistory.prototype.Start_Time = Functions.prototype.formatDateTime(message.timestamp);
                    Constants.prototype.response = new builder.Message()
                        .address(message.address);
                    setTimeout(() => {
                        bot.beginDialog(msgAddress, 'Home');
                    }, 500);
                }
            });
        }
    });
    //#endregion

    //#region 'Back Channel to detect browser close'
    bot.on('event', function (args) {
        if (args.name == 'Page Load') {        
            var arr = args.value.split('|');
            ChatHistory.prototype.Browser = arr[0];
            ChatHistory.prototype.Os = arr[1];
        }
        else if (args.name == 'Page Unload') {        
            ChatHistory.prototype.End_Time = Functions.prototype.formatDateTime(args.timestamp);
            Functions.prototype.savetoDB();

        }
    });
    //#endregion

};
