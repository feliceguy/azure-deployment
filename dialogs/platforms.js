var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var customCards = require('../cards/card');
var Platforms = require('../lib/platforms');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');

module.exports = function() {

    //#region 'Dialog - Platforms'
    bot.dialog('Platforms', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Platforms');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Platforms')).resolution.values[0];
                if (Constants.prototype.prediction == 'Platforms') {
                    Constants.prototype.bubbles = Platforms.prototype.getPlatformsMenu();
                    Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                        Functions.prototype.sendMessage([Platforms.prototype.getResponse()], session, results);
                        Functions.prototype.sendCard(msg, session);
                    });
                } else {
                    if (Constants.prototype.prediction == 'CMS') {
                        Constants.prototype.bubbles = Platforms.prototype.getCmsMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Platforms.prototype.getPlatformResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else if (Constants.prototype.prediction == 'CRM') {
                        Constants.prototype.bubbles = Platforms.prototype.getCrmMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Platforms.prototype.getPlatformResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else if (Constants.prototype.prediction == 'Ecommerce') {
                        Constants.prototype.bubbles = Platforms.prototype.getEcommerceMenu();
                        Functions.prototype.createBubbleButton(session,Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Platforms.prototype.getPlatformResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else {
                        Constants.prototype.parameters = Platforms.prototype.returnPlatformCard(Constants.prototype.prediction);
                        Constants.prototype.bubbles = new customCards.Basicthumb(session, Constants.prototype.parameters);
                        Constants.prototype.msg = new builder.Message(session).addAttachment(Constants.prototype.bubbles);
                        Functions.prototype.sendCard(Constants.prototype.msg, session);
                    }
                }
            } else {
                session.beginDialog('None');
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Platforms'
    });

};