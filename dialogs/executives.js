var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var CustomCards = require('../cards/card');
var Executives = require('../lib/executives');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');
var Videos = require('../videos/links');
module.exports = function () {

    //#region 'Dialog - Suyati Management Team(Executives)'
    // Leadership Team
    bot.dialog('Leadership Team', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Executives.prototype.getResponse('Leadership')], session, args);
            Constants.prototype.bubbles = Executives.prototype.getLeadershipMenu();
            Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                Functions.prototype.sendCard(msg, session);
            });
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Leadership Team'
    });

    // Management Team
    bot.dialog('Management Team', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Management Team');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Management Team')).resolution.values[0];
                if (Constants.prototype.prediction == 'Management') {
                    Functions.prototype.sendMessage([Executives.prototype.getResponse(Constants.prototype.prediction)], session, results);
                    Constants.prototype.bubbles = Executives.prototype.getManagementMenu();
                    Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                        Functions.prototype.sendCard(msg, session);
                    });
                } else {
                    Constants.prototype.parameters = Executives.prototype.returnExecutiveCard(Constants.prototype.prediction);
                        Constants.prototype.bubbles = new CustomCards.Thumbcard(session, Constants.prototype.parameters);
                        Constants.prototype.msg = new builder.Message(session).addAttachment(Constants.prototype.bubbles);
                        Functions.prototype.sendCard(Constants.prototype.msg, session);
                    if (Constants.prototype.prediction == 'CEO' || Constants.prototype.prediction == 'CPO') {
                        Functions.prototype.sendMessage([Executives.prototype.getResponse('Video')],session,results);
                        Constants.prototype.bubbles = new CustomCards.Videocard(Videos.prototype.getLink(Constants.prototype.prediction));
                        session.send(new builder.Message(session).addAttachment(Constants.prototype.bubbles));
                    }
                }
            } else {
                session.beginDialog('None');
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Management Team'
    });

    // Advisory Board
    bot.dialog('Advisory Board', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Executives.prototype.getResponse('Advisory')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Advisory Board'
    });
    //#endregion
};