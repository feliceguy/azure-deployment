var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var Resources = require('../lib/resources');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');

module.exports = function() {

    //#region 'Dialog - 'White Papers, Case Studies, Webinars, Blogs'
    bot.dialog('WP-CS', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'WP-CS');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'WP-CS')).resolution.values[0];
                if (Constants.prototype.prediction == 'WhitePaper') {
                    Constants.prototype.response = Resources.prototype.getResponse() + Resources.prototype.getResourceMenu();
                    Functions.prototype.saveTranscript('Bot', Constants.prototype.response, Functions.prototype.getDateTime());
                    builder.Prompts.choice(session, Resources.prototype.getResponse(), Resources.prototype.getResourceMenu(), {
                        listStyle: 3
                    });
                }
            } else {
                session.beginDialog('None');
            }
        }, (session, results) => {
            if (results.response) {
                Constants.prototype.response = results.response.entity;
            }
            builder.Prompts.choice(session, Resources.prototype.getResourceResponse(), Resources.prototype.geSubResourceMenu(), {
                listStyle: 3
            });
            Functions.prototype.saveTranscript('Bot', Resources.prototype.getResourceResponse() + Resources.prototype.geSubResourceMenu(), Functions.prototype.getDateTime());
        },
        (session, results) => {
            if (results.response.entity == 'White Papers') {
                Functions.prototype.sendMessage([Resources.prototype.returnResourceLink(Constants.prototype.response + '-White Papers')], session, results);
            } else if (results.response.entity == 'Case Study') {
                Functions.prototype.sendMessage([Resources.prototype.returnResourceLink(Constants.prototype.response + '-Case Study')], session, results);
            } else if (results.response.entity == 'Webinars') {
                Functions.prototype.sendMessage([Resources.prototype.returnResourceLink(Constants.prototype.response + '-Webinars')], session, results);
            } else if (results.response.entity == 'Blog') {
                Functions.prototype.sendMessage([Resources.prototype.returnResourceLink(Constants.prototype.response + '-Blog')], session, results);
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'WP-CS'
    });
    // #endregion
}