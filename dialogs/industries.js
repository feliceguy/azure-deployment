var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var customCards = require('../cards/card');
var Industry = require('../lib/industry');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');

module.exports = function() {

    //#region 'Dialog - Industries'
    bot.dialog('Industry', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Industry');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Industry')).resolution.values[0];
                if (Constants.prototype.prediction == 'Industry') {
                    Functions.prototype.sendMessage([Industry.prototype.getResponse()], session, results);
                    Constants.prototype.bubbles = Industry.prototype.getIndustriesMenu();
                    Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                        Functions.prototype.sendCard(msg, session);
                    });
                } else {
                    Constants.prototype.parameters = Industry.prototype.returnIndustryCard(Constants.prototype.prediction);
                    Constants.prototype.bubbles = new customCards.Basicthumb(session, Constants.prototype.parameters);
                    Constants.prototype.msg = new builder.Message(session).addAttachment(Constants.prototype.bubbles);
                    Functions.prototype.sendCard(Constants.prototype.msg, session);
                }
            } else {
                session.beginDialog('None');
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Industry'
    });
};