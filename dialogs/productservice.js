var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var customCards = require('../cards/card');
var ProductService = require('../lib/product-service');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');

module.exports = function() {

    //#region 'Dialog - 'Products and Services'
    bot.dialog('Products & Services', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Products & Services');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Products & Services')).resolution.values[0];
                if (Constants.prototype.prediction == 'Products & Services') {
                    Constants.prototype.bubbles = ProductService.prototype.getMenu();
                    Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                        Functions.prototype.sendMessage([ProductService.prototype.getResponse()], session, results);
                        Functions.prototype.sendCard(msg, session);
                    });
                } else {
                    if (Constants.prototype.prediction == 'Product') {
                        Constants.prototype.parameters = ProductService.prototype.returnProductCard(Constants.prototype.prediction);
                        Constants.prototype.bubbles = new customCards.Basicthumb(session, Constants.prototype.parameters);
                        Constants.prototype.msg = new builder.Message(session).addAttachment(Constants.prototype.bubbles);
                        Functions.prototype.sendCard(Constants.prototype.msg, session);
                    } else if (Constants.prototype.prediction == 'Service') {
                        Constants.prototype.bubbles = ProductService.prototype.getServicesMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([ProductService.prototype.getService()], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else {
                        Constants.prototype.parameters = ProductService.prototype.returnServicesCard(Constants.prototype.prediction);
                        Constants.prototype.bubbles = new customCards.Basicthumb(session, Constants.prototype.parameters);
                        Constants.prototype.msg = new builder.Message(session).addAttachment(Constants.prototype.bubbles);
                        Functions.prototype.sendCard(Constants.prototype.msg, session);
                    }
                }
            } else {
                session.beginDialog('None');
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Products & Services'
    });
    //#endregion

};