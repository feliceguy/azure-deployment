var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var customCards = require('../cards/card');
var Technologies = require('../lib/technologies');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');
 
module.exports = function() {
    //#region 'Dialog - Technologies'
    bot.dialog('Technologies', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Technologies');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Technologies')).resolution.values[0];
                if (Constants.prototype.prediction == 'Technology') {
                    Constants.prototype.bubbles = Technologies.prototype.getTechnologiesMenu();
                    Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                        Functions.prototype.sendMessage([Technologies.prototype.getResponse()], session, results);
                        Functions.prototype.sendCard(msg, session);
                    });
                } else {
                    if (Constants.prototype.prediction == 'Microsoft') {
                        Constants.prototype.bubbles = Technologies.prototype.getMicrosoftMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Technologies.prototype.getTechnologyResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else if (Constants.prototype.prediction == 'Open source') {
                        Constants.prototype.bubbles = Technologies.prototype.getOpensourceMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Technologies.prototype.getTechnologyResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else if (Constants.prototype.prediction == 'Mobile') {
                        Constants.prototype.bubbles = Technologies.prototype.getMobileMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Technologies.prototype.getTechnologyResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else if (Constants.prototype.prediction == 'Cloud') {
                        Constants.prototype.bubbles = Technologies.prototype.getCloudMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Technologies.prototype.getTechnologyResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else if (Constants.prototype.prediction == 'Big Data') {
                        Constants.prototype.bubbles = Technologies.prototype.getBigdataMenu();
                        Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function(msg) {
                            Functions.prototype.sendMessage([Technologies.prototype.getTechnologyResponse(Constants.prototype.prediction)], session, results);
                            Functions.prototype.sendCard(msg, session);
                        });
                    } else {
                        Constants.prototype.parameters = Technologies.prototype.returnTechnologycard(Constants.prototype.prediction);
                        Constants.prototype.bubbles = new customCards.Basicthumb(session, Constants.prototype.parameters);
                        Constants.prototype.msg = new builder.Message(session).addAttachment(Constants.prototype.bubbles);
                        Functions.prototype.sendCard(Constants.prototype.msg, session);
                    }
                }
            } else {
                session.beginDialog('None');
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Technologies'
    });
};