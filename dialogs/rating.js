var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var Store = require('../lib/store');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');

module.exports = function () {

    //#region 'Dialog - Rating'
    bot.dialog('Rating', [
        (session, results) => {
            Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
            Constants.prototype.rating = true;
            builder.Prompts.choice(session, Store.prototype.getResponse('Rate'), Store.prototype.getRating(), {
                listStyle: 3
            });
            Functions.prototype.storeRating('Bot', Store.prototype.getResponse('Rate') + Store.prototype.getRating(),
                Functions.prototype.getDateTime());
        },
        (session, results) => {
            Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
            Constants.prototype.rate = parseInt(results.response.entity);
            if (Constants.prototype.rate <= 3) {
                builder.Prompts.choice(session, Store.prototype.getRatingResponse(0), Store.prototype.getRatingList('lowRating'), {
                    listStyle: 3
                });
                Functions.prototype.storeRating('Bot', Store.prototype.getRatingResponse(0) + Store.prototype.getRatingList('lowRating'),
                    Functions.prototype.getDateTime());
            } else if (Constants.prototype.rate == 4) {
                builder.Prompts.choice(session, Store.prototype.getRatingResponse(1), Store.prototype.getRatingList('lowRating'), {
                    listStyle: 3
                });
                Functions.prototype.storeRating('Bot', Store.prototype.getRatingResponse(1) + Store.prototype.getRatingList('lowRating'),
                    Functions.prototype.getDateTime());
            } else if (Constants.prototype.rate >= 5) {
                builder.Prompts.choice(session, Store.prototype.getRatingResponse(2), Store.prototype.getRatingList('highRating'), {
                    listStyle: 3
                });
                Functions.prototype.storeRating('Bot', Store.prototype.getRatingResponse(1) + Store.prototype.getRatingList('highRating'),
                    Functions.prototype.getDateTime());
            }
        },
        (session, results) => {

            if (results.response) {
                if (results.response.entity == 'Others') {
                    Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
                    if (Constants.prototype.rate <= 3) {
                        builder.Prompts.text(session, Store.prototype.getOtherRatingResponse(0));
                        Functions.prototype.storeRating('Bot', Store.prototype.getOtherRatingResponse(0));
                    } else if (Constants.prototype.rate == 4) {
                        builder.Prompts.text(session, Store.prototype.getOtherRatingResponse(1));
                        Functions.prototype.storeRating('Bot', Store.prototype.getOtherRatingResponse(1));
                    } else if (Constants.prototype.rate == 5) {
                        builder.Prompts.text(session, Store.prototype.getOtherRatingResponse(2));
                        Functions.prototype.storeRating('Bot', Store.prototype.getOtherRatingResponse(2));
                    }
                } else {
                    session.endConversation();
                    session.beginDialog('Rating Improve');
                }
            }
        },
        (session, results) => {
            if (results.response) {
                session.endConversation();
                session.beginDialog('Rating Improve');
            }
        }
    ]).triggerAction({
        matches: 'Rating'
    });
    // Rating Improve
    bot.dialog('Rating Improve', [
        (session) => {
            Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
            builder.Prompts.choice(session, 'Can you tell me how I can improve?', ['Yes', 'Maybe later'], {
                listStyle: 3
            });
            Functions.prototype.storeRating('Bot', 'Can you tell me how I can improve?,Yes, Maybe later', Functions.prototype.getDateTime());
        },
        (session, results, next) => {
            if (results.response.entity == 'Yes') {
                Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
                builder.Prompts.text(session, 'How can I help you better?');
                Functions.prototype.storeRating('Bot', 'How can I help you better?', Functions.prototype.getDateTime());
            } else if (results.response.entity == 'Maybe later') {
                next();
            }
        },
        (session, results) => {
            Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendRatingMessage([Store.prototype.getResponse('End')], session);
        }
        // (session, results) => {
        //     Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
        //     builder.Prompts.choice(session, Store.prototype.getResponse('Else'), ['Yes', 'No'], {
        //         listStyle: 3
        //     });
        //     Functions.prototype.storeRating('Bot', Store.prototype.getResponse('Else'), Functions.prototype.getDateTime());
        // },
        // (session, results) => {
        //     Functions.prototype.storeRating('User', session.message.text, session.message.timestamp);
        //     if (results.response.entity == 'Yes') {
        //         session.beginDialog('Help');
        //     } else {
        //         Functions.prototype.sendMessage([Store.prototype.getResponse('End') + Store.prototype.getResponse('GoodDay')], session, results);
        //     }
        // }
    ]);
    //#endregion
};