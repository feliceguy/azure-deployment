var Careers = require('../lib/careers');
var Constants = require('../functions/constants');
var Functions = require('../functions/general');
var CustomCards = require('../cards/card');
var customCards = require('../cards/card');
var Videos = require('../videos/links');

module.exports = function () {
    
    //#region 'Dialog - Suyati Location'
    bot.dialog('Suyati.Location', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Suyati.Location');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.location = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Suyati.Location')).resolution.values[0];
                session.beginDialog('Jobs');
            } else {
                session.beginDialog('None');
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Suyati.Location'
    });
    //#endregion
    
    //#region 'Jobs/Careers'
    bot.dialog('Jobs', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (Constants.prototype.location != null) {
                Functions.prototype.sendMessage([Careers.prototype.getResponse(Constants.prototype.location)], session, results);
            }
            else {
                Functions.prototype.sendMessage([Careers.prototype.getResponse('Jobs')], session, results);
                Constants.prototype.bubbles = Careers.prototype.getLocation();
                Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                    Functions.prototype.sendCard(msg, session);
                    Constants.prototype.bubbles = new customCards.Videocard(Videos.prototype.getLink('Jobs'));
                    Functions.prototype.sendMessage([Careers.prototype.getResponse('Video')],session,results);
                    session.send(new builder.Message(session).addAttachment(Constants.prototype.bubbles));
                });
            }
            Constants.prototype.location = null;
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Jobs'
    });
    // #endregion
}

