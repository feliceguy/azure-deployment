var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var Store = require('../lib/store');
var CustomCards = require('../cards/card');
var Constants = require('../functions/constants');
var Functions = require('../functions/general');
var CustomCards = require('../cards/card');
var Videos = require('../videos/links');
var SalesMarketing = require('../lib/sales-marketing');
var ChatHistory = require('../functions/chathistory');
var Schedule = require('../functions/schedule');
var REGEXNAME = /\b(i'm|my name is|i am|im).(\w+)\b/ig;

module.exports = function () {
     //#region 'Dialog - About Suyati'
    bot.dialog('Suyati.Contact', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getCallbackResponse('Contact')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Suyati.Contact'
    });
    //#endregion


    //#region 'Dialog - Call back'
    bot.dialog('Callback', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Name'));
            Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Name'), Functions.prototype.getDateTime());
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response) {
                if (results.response.match(REGEXNAME)) {
                    var ans = REGEXNAME.exec(results.response);
                    Schedule.prototype.Name = ans[2];
                    ChatHistory.prototype.Name = ans[2];
                } else {
                    Schedule.prototype.Name = results.response;
                    ChatHistory.prototype.Name = results.response;
                }
                Functions.prototype.saveTranscript('Bot', Schedule.prototype.Name, Functions.prototype.getDateTime());
                Constants.prototype.salesResponse = 'Callback';
                session.endConversation();
                session.beginDialog('ValidateEmail');
            }
        }
    ]).triggerAction({
        matches: 'Callback'
    });
    //#endregion
    
    //#region 'Dialog - About Suyati'
    bot.dialog('Suyati.About', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('About')], session, args);
            Constants.prototype.bubbles = new CustomCards.Videocard(Videos.prototype.getLink('Suyati.About'));
            Functions.prototype.sendMessage([Store.prototype.getResponse('Video')], session, args);
            session.send(new builder.Message(session).addAttachment(Constants.prototype.bubbles));
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Suyati.About'
    });
    //#endregion

    //#region 'Dialog - Suyati's Business
    bot.dialog('Suyati.Business', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Partners')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Suyati.Business'
    });
    //#endregion
    
    //#region 'Dialog - Suyati's Business
    bot.dialog('Suyati.Origin', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Origin')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Suyati.Origin'
    });
    //#endregion    
    
      //#region 'Dialog - Suyati's Partners
    bot.dialog('Suyati.Partners', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Partners')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Suyati.Partners'
    });
    //#endregion

    //#region 'Dialog - Digital Transformation
    bot.dialog('Digital Transformation', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Digital Transformation')], session, args);
            Constants.prototype.bubbles = new CustomCards.Videocard(Videos.prototype.getLink('Digital Transformation'));
            Functions.prototype.sendMessage([Store.prototype.getResponse('Video')], session, args);
            session.send(new builder.Message(session).addAttachment(Constants.prototype.bubbles));
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Digital Transformation'
    });
    //#endregion

    //#region 'Dialog - Greeting, Help, Ideal, Thanks, None, Home'
    // None intent
    bot.dialog('None', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.response = Store.prototype.getNoneResponse();
            var randomItem = Constants.prototype.response[Math.floor(Math.random() * Constants.prototype.response.length)];
            Functions.prototype.sendMessage([randomItem], session, args);
        }
    ]).triggerAction({
        matches: 'None'
    });

    // Help
    bot.dialog('Help', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Help')], session, args);
            Constants.prototype.bubbles = Store.prototype.getMenu();
            Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                Functions.prototype.sendCard(msg, session);
            });
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Help'
    });

    // Home
    bot.dialog('Home', [
        (session, args) => {
            Functions.prototype.sendMessage([Store.prototype.getResponse('Greeting')], session, args);
            Constants.prototype.bubbles = Store.prototype.getMenu();
            Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                Functions.prototype.sendCard(msg, session);
            });
            session.endConversation();
        }
    ]);

    // Thanks
    bot.dialog('Thanks', [
        (session, args) => {
            if (Constants.prototype.rating == false) {
                session.replaceDialog('Rating');
            } else {
                Functions.prototype.sendMessage([Store.prototype.getResponse('Welcome')], session, args);
            }
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Thanks'
    });

    // Greeting intent
    bot.dialog('Greeting', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Greeting')], session, args);
            Constants.prototype.bubbles = Store.prototype.getMenu();
            Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                Functions.prototype.sendCard(msg, session);
            });
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Greeting'
    });

    // Idle user intent
    bot.dialog('Ideal', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Ideal')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Ideal'
    });
    // 'Okay'
    bot.dialog('Okay', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Functions.prototype.sendMessage([Store.prototype.getResponse('Okay')], session, args);
            session.endConversation();
        }
    ]).triggerAction({
        matches: 'Okay'
    });
    //#endregion
  
};