var Configuration = require('../config');
var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var datetimeParse = require('chrono-node');
var SalesMarketing = require('../lib/sales-marketing');
var MailSchedule = require('../lib/mailschedule');
var Store = require('../lib/store');
var Functions = require('../functions/general');
var Constants = require('../functions/constants');
var ChatHistory = require('../functions/chathistory');
var Schedule = require('../functions/schedule');
var REGEXNAME = /\b(i'm|my name is|i am|im).(\w+)\b/ig;
var REGEXEMAIL = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
var REGEXPHONE = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
var REGEXREPLACENULL = /(?:\\[rn]|[\r\n]+)+/g;
var REGEXEXPRESSION = [/&/g, /./g];
var REGEXREPLACEAND = /[&]/g;
var AGE = '01/01/2018';

module.exports = function () {

    //#region 'Dialog - 'Sales and marketing'
    bot.dialog('Sales & Marketing', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.entity = builder.EntityRecognizer.findEntity(results.intent.entities, 'Sales & Marketing');
            if (Constants.prototype.entity !== null) {
                Constants.prototype.prediction = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Sales & Marketing')).resolution.values[0];
                if (Constants.prototype.prediction == 'Schedule a Meeting') {
                    session.beginDialog('Schedule Meeting');
                } else if (Constants.prototype.prediction == 'Contact Sales Team') {
                    session.beginDialog('Contact Sales Team');
                }
            } else {
                Constants.prototype.bubbles = SalesMarketing.prototype.getMenu();
                Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
                    Functions.prototype.sendCard(msg, session);
                });
            }
        }
    ]).triggerAction({
        matches: 'Sales & Marketing'
    });
    //#endregion

    //#region 'Dialog - 'Sales and marketing - Schedule Meeting'
    bot.dialog('Schedule Meeting', [
        (session) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            Constants.prototype.response = SalesMarketing.prototype.getResponse('Schedule') + SalesMarketing.prototype.getScheduleMenu();
            builder.Prompts.choice(session, SalesMarketing.prototype.getResponse('Schedule'), SalesMarketing.prototype.getScheduleMenu(), {
                listStyle: 3
            });
            Functions.prototype.saveTranscript('Bot', Constants.prototype.response, Functions.prototype.getDateTime());
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response.entity == 'Reach out to me') {
                session.beginDialog('Reach out to me');
            } else if (results.response.entity == 'Schedule my own meeting') {
                session.endConversation();
                session.beginDialog('GetDate');
            }
        }
    ]).triggerAction({
        matches: 'Schedule Meeting'
    });

    // Schedule my own meeting
    bot.dialog('Schedule My Own Meeting', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Name'));
            Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Name'), Functions.prototype.getDateTime());
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response) {
                if (results.response.match(REGEXNAME)) {
                    var ans = REGEXNAME.exec(results.response);
                    Schedule.prototype.Name = ans[2];
                    ChatHistory.prototype.Name = ans[2];
                } else {
                    Schedule.prototype.Name = results.response;
                    ChatHistory.prototype.Name = results.response;
                }
                Functions.prototype.saveTranscript('Bot', Schedule.prototype.Name, Functions.prototype.getDateTime());
                Constants.prototype.salesResponse = 'Schedule My Own Meeting';
                session.endConversation();
                session.beginDialog('ValidateEmail');
            }
        }
    ]).triggerAction({
        matches: 'Schedule My Own Meeting'
    });
    //#endregion

    //#region 'Dialog - 'Sales and marketing - Reach out to me'
    bot.dialog('Reach out to me', [
        (session, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Name'));
            Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Name'), Functions.prototype.getDateTime());
        },
        (session, results, args) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response) {
                if (results.response.match(REGEXNAME)) {
                    var ans = REGEXNAME.exec(results.response);
                    Schedule.prototype.Name = ans[2];
                    ChatHistory.prototype.Name = ans[2];
                } else {
                    Schedule.prototype.Name = results.response;
                    ChatHistory.prototype.Name = results.response;
                }
                Functions.prototype.saveTranscript('Bot', Schedule.prototype.Name, Functions.prototype.getDateTime());
                Constants.prototype.salesResponse = 'Reach out to me';
                session.endConversation();
                session.beginDialog('ValidateEmail');
            }
        }
    ]).triggerAction({
        matches: 'Reach out to me'
    });
    //#endregion

    //#region 'Dialog - 'Sales and marketing - Contact Sales Team'
    bot.dialog('Contact Sales Team', [
        (session) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Name'));
            Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Name'), Functions.prototype.getDateTime());
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response.match(REGEXNAME)) {
                var ans = REGEXNAME.exec(results.response);
                Schedule.prototype.Name = ans[2];
                ChatHistory.prototype.Name = ans[2];
            } else {
                Schedule.prototype.Name = results.response;
                ChatHistory.prototype.Name = results.response;
            }
            Functions.prototype.saveTranscript('Bot', Schedule.prototype.Name, Functions.prototype.getDateTime());
            Constants.prototype.salesResponse = 'Contact Sales Team';
            session.endConversation();
            session.replaceDialog('ValidateEmail');
        }

    ]).triggerAction({
        matches: 'Contact Sales Team'
    });
    //#endregion

    //#region 'Dialog - 'Sales and marketing - Sales response'
    bot.dialog('Sales Response', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (Schedule.prototype.Phone === null && Schedule.prototype.Email == null) {
                if (Constants.prototype.salesResponse === 'None Intent' || Constants.prototype.salesResponse === 'Callback') {
                    Functions.prototype.sendMessage([Store.prototype.getResponse('Reachout')], session, results);
                } else {
                    Functions.prototype.sendMessage([SalesMarketing.prototype.getResponse('NoResponse')], session, results);
                }
                session.endConversation();
            }

            else {
                if (Constants.prototype.salesResponse == 'Contact Sales Team') {
                    Functions.prototype.sendMessage([SalesMarketing.prototype.getResponse('Thanks')], session, results);
                    Constants.prototype.response = 'Hello ' + Schedule.prototype.Name + ',<br><br> ' + Configuration.prototype.getMailDetails('Contactsales');
                    MailSchedule.prototype.sendUserMail(Schedule.prototype, Constants.prototype.response, Constants.prototype.salesResponse);
                    session.endConversation();
                    session.replaceDialog('Rating');
                } else if (Constants.prototype.salesResponse == 'Reach out to me') {
                    Functions.prototype.sendMessage([SalesMarketing.prototype.getResponse('Thank')], session, results);
                    Constants.prototype.response = 'Hello ' + Schedule.prototype.Name + ',<br><br> ' + Configuration.prototype.getMailDetails('Reachoutme');
                    MailSchedule.prototype.sendUserMail(Schedule.prototype, Constants.prototype.response, Constants.prototype.salesResponse);
                    session.endConversation();
                    session.replaceDialog('Rating');
                } else if (Constants.prototype.salesResponse == 'Schedule My Own Meeting') {
                    var date = moment(Schedule.prototype.Date + ' ' + Schedule.prototype.Time);
                    Constants.prototype.response = 'Thank You! A meeting has been scheduled at ' + date.format('hh:mm A') +
                    ' on ' + date.format('ll') + ' and an email has been sent to you with the meeting details.\r\n\r\nHave a good day!';
                    Functions.prototype.sendMessage([Constants.prototype.response], session, results);
                    ChatHistory.prototype.IsMeetingScheduled = 'Yes';
                    MailSchedule.prototype.scheduleMeeting(Schedule.prototype, function (res, err) {
                    });
                    session.endConversation();
                    session.replaceDialog('Rating');
                }
                else if (Constants.prototype.salesResponse === 'None Intent') {
                    Functions.prototype.sendMessage([Store.prototype.getResponse('Thank')], session, results);
                    MailSchedule.prototype.sendCallbackMail(Schedule.prototype, Constants.prototype.salesResponse);
                    session.endDialog();
                }
                else if (Constants.prototype.salesResponse === 'Callback') {
                    Functions.prototype.sendMessage([Store.prototype.getResponse('Thank')], session, results);
                    MailSchedule.prototype.sendCallbackMail(Schedule.prototype, Constants.prototype.salesResponse);
                    session.endDialog();
                }
            }
            Constants.prototype.salesResponse = null;
            Constants.prototype.isValid = false; 
        }
    ]);
    //#endregion

    //#region 'Dialog - Email, Phone, Date,Time Validation'
    // Getting Valid Date
    bot.dialog('GetDate', [
        (session) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (!(Constants.prototype.isValid)) {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Date'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Date'));
            } else {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('InvalidDate'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('InvalidDate'));
            }
        }, (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response) {
                var dt;
                Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
                var isToday = datetimeParse.parseDate(results.response);

                if (isToday != null && isToday.toLocaleDateString() == new Date().toLocaleDateString() || results.response.toLowerCase() == 'tomorrow') {
                    dt = datetimeParse.casual.parseDate(results.response);
                }
                else {
                    dt = datetimeParse.strict.parseDate(results.response);
                }

                if (dt == null) {
                    Constants.prototype.isValid = true;
                    session.replaceDialog('GetDate');
                } else {
                    Schedule.prototype.Date = dt.toLocaleDateString();
                    Functions.prototype.validDateTime(dt, ' ', 'Date', function (results, error) {
                        if (results) {
                            if (parseInt(results) >= 0) {
                                Constants.prototype.isValid = false;
                                session.endConversation();
                                session.replaceDialog('GetTime');
                            } else {
                                Constants.prototype.isValid = true;
                                session.replaceDialog('GetDate');
                            }
                        } else {
                            Constants.prototype.isValid = true;
                            session.replaceDialog('GetDate');
                        }
                    });
                }
            }
        }
    ]).triggerAction({
        matches: 'GetDate'
    });

    // Getting Valid Time
    bot.dialog('GetTime', [
        (session) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (!(Constants.prototype.isValid)) {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Time'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Time'));
            } else {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('InvalidTime'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('InvalidTime'));
            }
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            var dt = datetimeParse.parseDate(results.response);
            if (dt == null) {
                Constants.prototype.isValid = true;
                session.replaceDialog('GetTime');
            } else {
                Schedule.prototype.Time = dt.toLocaleTimeString();
                Functions.prototype.validDateTime(Schedule.prototype.Date, dt.toLocaleTimeString(), 'DateTime', function (result, error) {
                    if (result) {
                        if (parseInt(result) >= 1) {
                            session.endConversation();
                            Constants.prototype.isValid = false;
                            session.endConversation();
                            session.beginDialog('Schedule My Own Meeting');
                        } else {
                            Constants.prototype.isValid = true;
                            session.replaceDialog('GetTime');
                        }
                    } else {
                        Constants.prototype.isValid = true;
                        session.replaceDialog('GetTime');
                    }
                });
            }
        }
    ]).triggerAction({
        matches: 'GetTime'
    });

    // Email Validation
    bot.dialog('ValidateEmail', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (!(Constants.prototype.isValid)) {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('Email'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Email'));
            } else {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('InvalidEmail'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('InvalidEmail'));
            }
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response.match(REGEXEMAIL)) {
                Schedule.prototype.Email = session.message.text;
                ChatHistory.prototype.Email = session.message.text;
                Constants.prototype.isValid = false;
                Functions.prototype.saveTranscript('Bot', Schedule.prototype.Email, Functions.prototype.getDateTime());
                session.replaceDialog('ValidatePhone');
                Constants.prototype.counter = 1;
            } else {
                if (Constants.prototype.counter == 2) {
                    Constants.prototype.isValid = false;
                    session.endConversation();
                    session.replaceDialog('ValidatePhone');
                    Constants.prototype.counter = 1;
                } else {
                    Constants.prototype.counter = 2;
                    Constants.prototype.isValid = true;
                    session.replaceDialog('ValidateEmail', {
                        reprompt: true
                    });
                }
            }
        }
    ]);

    // Phone Number Validation
    bot.dialog('ValidatePhone', [
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (!(Constants.prototype.isValid)) {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('InvalidEmail'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('Phone'));
            } else {
                Functions.prototype.saveTranscript('Bot', SalesMarketing.prototype.getResponse('InvalidPhone'), Functions.prototype.getDateTime());
                builder.Prompts.text(session, SalesMarketing.prototype.getResponse('InvalidPhone'));
            }
        },
        (session, results) => {
            Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
            if (results.response) {
                ChatHistory.prototype.Phone = session.message.text;
                if (results.response.match(REGEXPHONE)) {
                    Schedule.prototype.Phone = session.message.text;
                    Constants.prototype.isValid = false;
                    Functions.prototype.saveTranscript('Bot', Schedule.prototype.Phone, Functions.prototype.getDateTime());
                    session.replaceDialog('Sales Response');
                    Constants.prototype.counter = 1;
                } else {
                    if (Constants.prototype.counter == 2) {
                        Constants.prototype.isValid = false;
                        session.endConversation();
                        session.replaceDialog('Sales Response');
                        Constants.prototype.counter = 1;
                    } else {
                        Constants.prototype.counter = 2;
                        Constants.prototype.isValid = true;
                        session.replaceDialog('ValidatePhone', {
                            reprompt: true
                        });
                    }
                }
            }
        }
    ]);
    //#endregion
};