var moment = require('moment');
moment.suppressDeprecationWarnings = true;
var Store = require('../lib/store'); 
var Functions = require('../functions/general');
var Constants = require('../functions/constants');
var log4js = require('log4js');
var logger = log4js.getLogger();

module.exports = function () {
	
//#region 'Dialog - Bot Details'	
//Bot Age
	bot.dialog('Bot.Age', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Age')], session, args);
			session.send()
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Age'
	});

	// Bot Name
	bot.dialog('Bot.Name', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Name')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Name'
	});

	// Bot Appearance
	bot.dialog('Bot.Appearance', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Appearance')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Appearance'
	});

	// Bot Hobbies
	bot.dialog('Bot.Hobbies', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Hobbies')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Hobbies'
	});

	// Bot Human
	bot.dialog('Bot.Human', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Human')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Human'
	});

	// Bot Human
	bot.dialog('Bot.Language', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Language')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Language'
	});

	// Bot Location
	bot.dialog('Bot.Location', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Location')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Location'
	});

	// Bot Locality
	bot.dialog('Bot.Locality', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Locality')], session, args);
			Constants.prototype.bubbles = Store.prototype.getMenu();
			Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
				Functions.prototype.sendCard(msg, session);
			});
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Locality'
	});

	// Bot State
	bot.dialog('Bot.State', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('State')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.State'
	});

	// Bot Skills
	bot.dialog('Bot.Skills', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Skills')], session, args);
			Constants.prototype.bubbles = Store.prototype.getMenu();
			Functions.prototype.createBubbleButton(session, Constants.prototype.bubbles, function (msg) {
				Functions.prototype.sendCard(msg, session);
			});
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Skills'
	});

	// Bot Skills
	bot.dialog('Bot.Gender', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			Functions.prototype.sendMessage([Store.prototype.getBotResponse('Gender')], session, args);
			session.endConversation();
		}
	]).triggerAction({
		matches: 'Bot.Gender'
	});

	// Bye
	bot.dialog('Bye', [
		(session, args) => {
			Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
			if (Constants.prototype.rating == false) {
				session.endConversation();
				session.beginDialog('Rating');
			}
			else {
				Functions.prototype.saveTranscript('User', session.message.text, session.message.timestamp);
				Functions.prototype.sendMessage([Store.prototype.getResponse('Bye')], session, args);
			}
		}
	]).triggerAction({
		matches: 'Bye'
	});
	//#endregion
};