class Schedule {

	constructor() {
		this.Date=null;
		this.Time=null;
		this.Topic=null;
		this.Name=null;
		this.Email=null;
		this.Phone=null;
	}
	
	set Date(Date) {
		this._Date = Date;
	}
	get Date() {
		return this._Date;
	}
	
	set Time(Time) {
		this._Time = Time;
	}
	get Time() {
		return this._Time;
	}
	
	set Topic(Topic) {
		this._Topic = Topic;
	}
	get Topic() {
		return this._Topic;
	}
	
	set Name(Name) {
		this._Name = Name;
	}
	get Name() {
		return this._Name;
	}
	
	set Email(Email) {
		this._Email = Email;
	}
	get Email() {
		return this._Email;
	}
	
	set Phone(Phone) {
		this._Phone = Phone;
	}
	get Phone() {
		return this._Phone;
	}
	
}
	module.exports=Schedule;