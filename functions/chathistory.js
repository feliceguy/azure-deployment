class ChatHistory {

	constructor() {
		this._id= null;
		this.Name= null;
		this.Email= null;
		this.Phone= null;
		this.Chat_Transcript= null;
		this.Start_Time= null;
		this.Location= null;	
		this.IsMeetingScheduled= null;
		this.IdleTime= null;
		this.IPaddress= null;
		this.Browser= null;
		this.End_Time= null;
		this.Os= null;
		this.Rating_Transcript= null;
		this.Chat_Training= null;
	}

	set Chat_Training(Chat_Training) {
		this._Chat_Training = Chat_Training;
	}
	get Chat_Training() {
		return this._Chat_Training;
	}
	set Name(Name) {
		this._Name = Name;
	}
	get Name() {
		return this._Name;
	}

	set Email(Email) {
		this._Email = Email;
	}
	get Email() {
		return this._Email;
	}

	set Phone(Phone) {
		this._Phone = Phone;
	}
	get Phone() {
		return this._Phone;
	}

	set Chat_Transcript(Chat_Transcript) {
		this._Chat_Transcript = Chat_Transcript;
	}
	get Chat_Transcript() {
		return this._Chat_Transcript;
	}

	set Start_Time(Start_Time) {
		this._Start_Time = Start_Time;
	}
	get Start_Time() {
		return this._Start_Time;
	}

	set Location(Location) {
		this._Location = Location;
	}
	get Location() {
		return this._Location;
	}

	set IsMeetingScheduled(IsMeetingScheduled) {
		this._IsMeetingScheduled = IsMeetingScheduled;
	}
	get IsMeetingScheduled() {
		return this._IsMeetingScheduled;
	}

	set IdleTime(IdleTime) {
		this._IdleTime = IdleTime;
	}
	get IdleTime() {
		return this._IdleTime;
	}
	set IPaddress(IPaddress) {
		this._IPaddress = IPaddress;
	}
	get IPaddress() {
		return this._IPaddress;
	}
	set Browser(Browser) {
		this._Browser = Browser;
	}
	get Browser() {
		return this._Browser;
	}
	set End_Time(End_Time) {
		this._End_Time = End_Time;
	}
	get End_Time() {
		return this._End_Time;
	}
	set Os(Os) {
		this._Os = Os;
	}
	get Os() {
		return this._Os;
	}
	set Rating_Transcript(Rating_Transcript) {
		this._Rating_Transcript = Rating_Transcript;
	}
	get Rating_Transcript() {
		return this._Rating_Transcript;
	}

};

module.exports = ChatHistory;