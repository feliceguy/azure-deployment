class Constants {

	constructor() {
		this.timerId = null;
		this.location = null;
		this.date = null;
		this.parameters = null;
		this.entity = null;
		this.response = null;
		this.historyTranscript = null;
		this.trainingTranscript = null;
		this.ratingArray = null;
		this.collection = null;
		this.prediction = null;
		this.bubbles = null;
		this.msgAddress = null;
		this.resetTimer = null;
		this.timer = null;
		this.flag = false;
		this.msg = null;
		this.rating = false;
		this.rate = null;
		this.isValid = false;
		this.salesResponse = null;
		this.counter = 1;
	}

	set timerId(timerId) {
		this._timerId = timerId;
	}
	get timerId() {
		return this._timerId;
	}

	set location(location) {
		this._location = location;
	}
	get location() {
		return this._location;
	}

	set date(date) {
		this._date = date;
	}
	get date() {
		return this._date;
	}

	set parameters(parameters) {
		this._parameters = parameters;
	}
	get parameters() {
		return this._parameters;
	}

	set entity(entity) {
		this._entity = entity;
	}
	get entity() {
		return this._entity;
	}

	set response(response) {
		this._response = response;
	}
	get response() {
		return this._response;
	}

	set historyTranscript(historyTranscript) {
		this._historyTranscript = historyTranscript;
	}
	get historyTranscript() {
		return this._historyTranscript;
	}

	set trainingTranscript(trainingTranscript) {
		this._trainingTranscript = trainingTranscript;
	}
	get trainingTranscript() {
		return this._trainingTranscript;
	}

	set ratingArray(ratingArray) {
		this._ratingArray = ratingArray;
	}
	get ratingArray() {
		return this._ratingArray;
	}

	set collection(collection) {
		this._collection = collection;
	}
	get collection() {
		return this._collection;
	}

	set prediction(prediction) {
		this._prediction = prediction;
	}
	get prediction() {
		return this._prediction;
	}

	set bubbles(bubbles) {
		this._bubbles = bubbles;
	}
	get bubbles() {
		return this._bubbles;
	}

	set msgAddress(msgAddress) {
		this._msgAddress = msgAddress;
	}
	get msgAddress() {
		return this._msgAddress;
	}

	set resetTimer(resetTimer) {
		this._resetTimer = resetTimer;
	}
	get resetTimer() {
		return this._resetTimer;
	}

	set timer(timer) {
		this._timer = timer;
	}
	get timer() {
		return this._timer;
	}

	set flag(flag) {
		this._flag = flag;
	}
	get flag() {
		return this._flag;
	}

	set msg(msg) {
		this._msg = msg;
	}
	get msg() {
		return this._msg;
	}

	set rating(rating) {
		this._rating = rating;
	}
	get rating() {
		return this._rating;
	}

	set rate(rate) {
		this._rate = rate;
	}
	get rate() {
		return this._rate;
	}

	set isValid(isValid) {
		this._isValid = isValid;
	}
	get isValid() {
		return this._isValid;
	}

	set salesResponse(salesResponse) {
		this._salesResponse = salesResponse;
	}
	get salesResponse() {
		return this._salesResponse;
	}

	set counter(counter) {
		this._counter = counter;
	}
	get counter() {
		return this._counter;
	}
	set schedule(schedule) {
		this._schedule = schedule;
	}
	get schedule() {
		return this._schedule;
	}

};
module.exports = Constants;

 
	 