var Configuration = require('../config');
var moment = require('moment-timezone');
moment.suppressDeprecationWarnings = true;
var ObjectID = require('mongodb').ObjectID;
var mongoClient = require('mongodb').MongoClient;
var datetimeParse = require('chrono-node');
var Constants = require('./constants');
var Store = require('../lib/store');
var Schedule = require('./schedule');
var ChatHistory = require('./chathistory');
var REGEXNAME = /\b(i'm|my name is|i am|im).(\w+)\b/ig;
var REGEXEMAIL = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
var REGEXPHONE = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
var REGEXREPLACENULL = /(?:\\[rn]|[\r\n]+)+/g;
var REGEXEXPRESSION = [/&/g, /./g];
var REGEXREPLACEAND = /[&]/g;
var counter;
var prevIntent;


class General {    
    //#region 'Creating hero card based bubbled button'
    createBubbleButton(session, parameters, callback) {
        var msg = new builder.Message(session);
        var collection = [];
        msg.attachmentLayout(builder.AttachmentLayout.list);
        for (var i = 0; i < parameters.length; i++) {
            if (parameters[i].match(REGEXEXPRESSION[0])) {
                Constants.prototype.response = parameters[i].replace(REGEXREPLACEAND, ' and ');
            } else if (parameters[i].match(REGEXEXPRESSION[1])) {
                Constants.prototype.response = parameters[i].replace(REGEXREPLACENULL, ' ');
            }
            collection.push(new builder.HeroCard(session, Constants.prototype.response)
                .buttons([
                builder.CardAction.imBack(session, Constants.prototype.response, parameters[i])
            ]));
        }
        msg.attachments(collection);
        callback(msg);
    }
    //#endregion

    //#region 'Storing Transcript in an array'
    saveTranscript(identity, message, time, args) {
        switch (identity) {
            case 'Bot':
                if (time == null) {
                    Constants.prototype.historyTranscript += identity + ':' + message + '|';
                } else {
                    if (Constants.prototype.historyTranscript === undefined || Constants.prototype.historyTranscript === null) {
                        Constants.prototype.historyTranscript = identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                    } else {
                        Constants.prototype.historyTranscript += identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                    }
                }
                if (args != null && args != null) {
                    if (args.hasOwnProperty('intent')) {
                        Constants.prototype.trainingTranscript += 'Intent : ' + args.intent.intent + '|' + 'Intent Score : '
                        + args.intent.score + '|';
                        if (args.intent.hasOwnProperty('entities')) {
                            Constants.prototype.entity = args.intent.entities.length != 0 ? args.intent.entities[0].entity : null;
                            var entityScore = args.intent.entities.length != 0 ? args.intent.entities[0].score : null;

                            Constants.prototype.trainingTranscript += ' Entity : ' + Constants.prototype.entity + '|' + 'Entity Score : ' + entityScore + '|'
                            + identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                        }
                    }
                    else if (args.hasOwnProperty('response')) {
                        Constants.prototype.trainingTranscript += ' Entity : ' + args.response.entity + '|' + 'Entity Score : ' + args.response.score + '|'
                        + identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                    }
                }
                break;
            case 'User':
                if (Constants.prototype.historyTranscript === undefined || Constants.prototype.historyTranscript === null) {
                    Constants.prototype.historyTranscript = identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                } else {
                    Constants.prototype.historyTranscript += identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                }
                if (Constants.prototype.trainingTranscript === null || Constants.prototype.trainingTranscript === undefined) {
                    Constants.prototype.trainingTranscript = identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                } else {
                    Constants.prototype.trainingTranscript += identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                }
                break;
        }
    }
    //#endregion

    //#region 'Saving Chat details into Mongodb' 
    savetoDB() {
        if (Constants.prototype.trainingTranscript !== null && Constants.prototype.trainingTranscript !== undefined) {
            ChatHistory.prototype.Chat_Training = Constants.prototype.trainingTranscript.replace(REGEXREPLACENULL, '');
        }
        if (Constants.prototype.historyTranscript != undefined || Constants.prototype.historyTranscript != null) {
            ChatHistory.prototype.Chat_Transcript = Constants.prototype.historyTranscript.replace(REGEXREPLACENULL, '');
        }
        if (Constants.prototype.ratingArray != undefined || Constants.prototype.ratingArray != null) {
            ChatHistory.prototype.Rating_Transcript = Constants.prototype.ratingArray.replace(REGEXREPLACENULL, '');
        }
        ChatHistory.prototype.IdleTime = moment(ChatHistory.prototype.Start_Time).diff(moment(ChatHistory.prototype.End_Time), 'minutes');
        mongoClient.connect(Configuration.prototype.getDatabaseDetails('Endpoint'), {
            useNewUrlParser: true
        }, function (err, connection) {
                console.log('DB connection open');
                var db = connection.db(Configuration.prototype.getDatabaseDetails('DatabaseId'));
                ChatHistory.prototype._id = new ObjectID();
                try {
                    db.collection(Configuration.prototype.getDatabaseDetails('HistoryCollectionId'))
                        .save(ChatHistory.prototype, function (err, res) {
                        if (res) {
                            console.log('Document Saved to Database Successfully');
                        } else {
                            console.log('Database Error ', err);
                        }
                    });
                } catch (err) {
                    console.log('Database Error ', err);
                }
                finally {
                    console.log('DB connection closed');
                    connection.close();
                    General.prototype.initialise();
                }
            });
        // }
    }     
    //#endregion

    //#region 'Get Date/time and Validate Date/Time'
    getDateTime() {
        Constants.prototype.date = new Date();
        return moment(Constants.date).tz("Asia/Colombo").format('MM/DD/YYYY hh:mm:ss A');
    }

    //#region 'Format Date/Time'
    formatDateTime(date) {
        return moment(date).tz("Asia/Colombo").format('MM/DD/YYYY hh:mm:ss A');
    }

    // Function - Validate Date and Time
    validDateTime(_date, _time, parameter, callback) {
        var startDate = new Date(_date);
        var date = new Date();
        date.setHours(date.getHours() + 5);
        date.setMinutes(date.getMinutes() + 30);

        if (parameter == 'Date') {
            var isToday = moment(startDate.toLocaleDateString()).isSame(moment(date.toLocaleDateString()));
            if (isToday) {
                callback(1);
            } else {
                callback(moment(startDate.toLocaleDateString()).diff(date.toLocaleDateString(), 'days'));
            }
        } else if (parameter == 'DateTime') {
            startDate = datetimeParse.parseDate(_time, _date);
            callback(moment(startDate).diff(moment(date), 'hours'));
        }
    }
    //#endregion
            
    //#region 'Sends message to user'
    sendMessage(response, session, parameters) {
        session.sendTyping();
        if (parameters != undefined && parameters.intent != undefined) {
            if (parameters.intent.intent === 'None' && prevIntent === parameters.intent.intent) {
                counter += 1;
                console.log(counter);
                if (counter === 2) {
                    session.send(Store.prototype.getResponse('None'));
                    General.prototype.saveTranscript('Bot', Store.prototype.getResponse('None'), null, parameters);
                    counter = 0;
                    Constants.prototype.isValid = false;
                    prevIntent = null;
                    Constants.prototype.salesResponse = 'None Intent';
                    session.replaceDialog('ValidateEmail');
                    return;
                }
            }
            prevIntent = parameters.intent.intent;
        }
        for (var i = 0; i < response.length; i++) {
            if (response.length == i + 1) {
                session.send(response[i]).endDialog();
                General.prototype.saveTranscript('Bot', response[i], General.prototype.getDateTime(), parameters);
            } else {
                session.send(response[i]);
                General.prototype.saveTranscript('Bot', response[i], null, parameters);
            }
        }
    }
    //#endregion

    //#region 'Sends message to user'
    sendRatingMessage(response, session) {
        for (var i = 0; i < response.length; i++) {
            if (response.length == i + 1) {
                session.send(response[i]).endDialog();
                General.prototype.storeRating('Bot', response[i], General.prototype.getDateTime());
            } else {
                session.send(response[i]);
                General.prototype.storeRating('Bot', response[i], null);
            }
        }
    }
    //#endregion

    //#region 'Store Rating'
    storeRating(identity, message, time, args) {
        switch (identity) {
            case 'Bot':
                if (time == null) {
                    Constants.prototype.ratingArray += identity + ':' + message + '|';
                } else {
                    Constants.prototype.ratingArray += identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                }
                if (args != null && args != undefined) {
                    Constants.prototype.entity = args.intent.entities.length > 0 ? args.intent.entities[0].entity : null;
                    var entityScore = args.intent.entities.length > 0 ? args.intent.entities[0].score : null;
                    Constants.prototype.trainingTranscript += 'Intent : ' + args.intent.intent + '|' + 'Intent Score : ' + args.intent.score + '|' + ' Entity : ' + Constants.prototype.entity + '|' +
                    'Entity Score : ' + entityScore + '|' + identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                }
                break;
            case 'User':
                var date = new Date(time);
                date.setHours(date.getHours() + 5);
                date.setMinutes(date.getMinutes() + 30);
                if (Constants.prototype.ratingArray === undefined || Constants.prototype.ratingArray === null) {
                    Constants.prototype.ratingArray = identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                } else {
                    Constants.prototype.ratingArray += identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                }
                if (Constants.prototype.trainingTranscript === null || Constants.prototype.trainingTranscript === undefined) {
                    Constants.prototype.trainingTranscript = identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                } else {
                    Constants.prototype.trainingTranscript += identity + ':' + message + '|' + General.prototype.formatDateTime(time) + '|';
                }
                break;
        }
    }
    //#endregion

    //#region 'Sends card to user' 
    sendCard(card, session) {
        Constants.prototype.response = ' Card: ';
        card.data.attachments.forEach(function (data) {
            if (data.content.hasOwnProperty('title')) {
                Constants.prototype.response += ' Title: ' + data.content.title + ' ';
            }
            if (data.content.hasOwnProperty('subtitle')) {
                Constants.prototype.response += ' Subtitle: ' + data.content.subtitle + ' ';
            }
            if (data.content.hasOwnProperty('text')) {
                Constants.prototype.response += ' Content: ' + data.content.text + ' ';
            }
            if (data.content.buttons[0].hasOwnProperty('title')) {
                Constants.prototype.response += ' Title: ' + data.content.buttons[0].title;
            }
        });
        session.send(card);
        General.prototype.saveTranscript('Bot', Constants.prototype.response, General.prototype.getDateTime());
        session.endDialog();
    }
    //#endregion
    initialise() {
        counter = 0;
        Constants.prototype.salesResponse = null;
        Constants.prototype.historyTranscript = null;
        Constants.prototype.ratingArray = null;
        Constants.prototype.trainingTranscript = null;
        ChatHistory.prototype.Chat_Training = null;
        ChatHistory.prototype.Chat_Transcript = null;
        ChatHistory.prototype.Rating_Transcript = null;
        ChatHistory.prototype.Name = null;
        ChatHistory.prototype.Email = null;
        ChatHistory.prototype.Phone = null;
        ChatHistory.prototype.Start_Time = null;
        ChatHistory.prototype.Location = null;
        ChatHistory.prototype.IsMeetingScheduled = null;
        ChatHistory.prototype.IdleTime = null;
        ChatHistory.prototype.Browser = null;
        ChatHistory.prototype.End_Time = null;
        ChatHistory.prototype.Os = null;
        Schedule.prototype.Phone = null;
        Schedule.prototype.Email = null;
        Constants.prototype.isValid = false;
    }
};

module.exports = General;