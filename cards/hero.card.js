function createhero(session, parameters) {
  
return {
    contentType: "application/vnd.microsoft.card.hero",
        content: {
            "buttons": [
                {
                    "type" :  "imBack",
                    "title": parameters.title,
                    "value": "Who is your" + parameters.title + "?",
                }
            ]}
        };
}

module.exports = 
{
   Herocard: createhero
};