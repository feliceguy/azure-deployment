
var createthumbcard = require("./thumb.card");
var createbasicthumb = require("./thumb.card");
var createherocard = require("./hero.card");
var createvideocard = require("./video.card");

module.exports = {

    Thumbcard: createthumbcard.Thumbcard,
    Videocard: createvideocard.Videocard,
    Herocard: createherocard.Herocard,
    Basicthumb: createbasicthumb.Basicthumb


};