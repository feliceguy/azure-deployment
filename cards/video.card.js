
function createvideo(parameter) {
    return {
		contentType: "application/vnd.microsoft.card.video",
		content: {
			media: [
				{ url: parameter, profile: "videocard" }
			],
			autostart: true,
			autoloop: true
		}

	};
}



module.exports =
{
	Videocard: createvideo
};