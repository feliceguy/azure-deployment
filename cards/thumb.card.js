function createthumb(session, parameters) {

    return {
        contentType: "application/vnd.microsoft.card.thumbnail",
        content: {
            "title": parameters.title,
            "subtitle":  parameters.subtitle,
            "text":  parameters.text,
            "images": [{
                "url":  parameters.img,
                "alt":  parameters.alt 
            }],
             
            "buttons": [
              {
                "type": "openUrl",
                "title": "Know more...",
                "value": "https://suyati.com/about-us/management-team/"
              }
            ]
                       
        }
    };
}

function basicthumb(session, parameters) {

    return {
        contentType: "application/vnd.microsoft.card.thumbnail",
        content: {
            "title": parameters.title,
            "subtitle":  parameters.subtitle,
            "text":  parameters.text,
            "images": [{
                "url":  parameters.img,
                "alt":  parameters.alt 
           }],  
            "buttons": [
              {
                "type": "openUrl",
                "title": "Know More / Download Resources",
                "value": parameters.url
              }
            ]                     
        }
    };
}

module.exports = 
{
   Thumbcard: createthumb,
   Basicthumb:basicthumb
};