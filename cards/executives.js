var builder = require('botbuilder');
var customCards = require("./card");

module.exports = [
    /* Executives */
    (session, results) => {
        var Predict = (builder.EntityRecognizer.findEntity(results.intent.entities, 'Executives')).resolution.values[0];
        /* CEO */
        if (Predict == 'ceo') {
            var para = {
                title: "Mukund Krishna",
                subtitle: 'Founder, CEO',
                text: 'He holds an MS from the State University of New York at Buffalo, and an MBA from Indiana University’s \
                        Kelley School of Business.',
                img: 'http://www.mukundkrishna.com/wp-content/uploads/2017/03/mk-large-img.png',
                alt: 'Chief Executive Officer'
            };
            var tcard = new customCards.thumbcard(session, para);
            var msg = new builder.Message(session).addAttachment(tcard);
            session.send(msg).endDialog();

        }
        /* CDO */
        else if (Predict == 'cdo') {
            var para = {
                title: 'Praveen Parameswaran',
                subtitle: 'Chief Delivery Officer',
                text: 'He holds a Bachelors Degree in Computer Science from Gogte Institute of Technology.',
                img: 'http://bridge.ictacademy.in/nellai2013/images/Praveen-Parameswaran.jpg',
                alt: 'Chief Executive Officer'
            } ;           
            var tcard = new customCards.thumbcard(session, para);
            var msg = new builder.Message(session).addAttachment(tcard);
            session.send(msg).endDialog();
        }
        /* CPO */
        else if (Predict == 'cpo') {
          var para = {
                title: 'Vincent N P',
                subtitle: 'Director—Human Resources',
                text: 'He has a Master’s Degree in Human Resource Management from Rajagiri College of Social Sciences. \r\n\r\n hr@suyati.com',
                img: 'https://i.ytimg.com/vi/P5rNCM8krDQ/hqdefault.jpg',
                alt: 'Chief Executive Officer'
            } ;           
            var tcard = new customCards.thumbcard(session, para);
            var msg = new builder.Message(session).addAttachment(tcard);
            session.send(msg).endDialog();
        }
        /* CTO */
        else if (Predict = 'cto') {
            var para = {
                title: 'S Karthikeyan',
                subtitle: 'Chief Technology Officer',
                text: 'Holds an Advanced Certificate in Information Technology Management from Indian Institute of Management, Kozhikode',
                img: 'http://suyati.com/wp-content/themes/simple-bootstrap/landing-page/img/KarthikPhotoBw.png',
                alt: 'Chief Technology Officer'
            } ;           
            var tcard = new customCards.thumbcard(session, para);
            var msg = new builder.Message(session).addAttachment(tcard);
            session.send(msg).endDialog();
        }         
    }
];



