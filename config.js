class Configuration {

	getLuisDetails(parameter) {
		var luisDetails = {
			appId: '4c6479f1-4f1a-417b-9091-4a6330e6dc49',
			apiKey: '5eb303af607b41fcbaa1c4f9fa57b925',
			apiHost: process.env.LuisAPIHostName || 'eastus.api.cognitive.microsoft.com'
		};
		return luisDetails[parameter];
	}

	
	getDatabaseDetails(parameter) {
		var databaseDetails =
			{
				Endpoint: 'mongodb://13.68.198.214:27017/',
				DatabaseId:'mekbot',
				HistoryCollectionId: 'ChatHistory'
			};
		return databaseDetails[parameter];
	}

	getMailDetails(parameter) {
		var sendMail =
			{
				From: 'services@suyati.com',
				To: 'services@suyati.com',
				ScheduleSubject: 'Meeting with Suyati',
				Subject: 'CB-Meeting',
				OrganizerName: 'Suyati Sales team',
				Organizeremail: 'services@suyati.com',
				Description: 'Meeting with Suyati',
				Location: 'Tele Meeting',
				Summary: 'Meeting with Suyati',
				Timezone: 'asia/kolkata',
				Reachoutme: ' Thanks for reaching out to us! Our Sales Team will contact you shortly to schedule a meeting. \
				  <br><br> Have a good day! <br><br> Thanks & Regards, <br> Suyati Sales & Marketing Team',
				Contactsales: ' Thanks for reaching out to us! Our Sales Team will contact you shortly. \
				  <br><br> Have a good day! <br><br> Thanks & Regards, <br> Suyati Sales & Marketing Team'

			};
		return sendMail[parameter];
	}

	getContactSalesTeam(parameter) {

		var contactSalesTeam = {
			Body: 'Hello Team, <br><br> A Mekbot user has requested you to reach out to them. The user details are given below, kindly contact the person.<br><br> ',
			Purpose: 'Contact Sales Team',
			Signature: ' Thanks & Regards, <br> Mekbot',
			Subject: ' CB-Contact'
		};
		return contactSalesTeam[parameter];
	}

	getReachOutMe(parameter) {
		var reachOutMe = {
			Body: 'Hello Team, <br><br> A Mekbot user has requested you to contact them to schedule a meeting. The user details are given below, kindly contact the person.<br><br> ',
			Purpose: 'Schedule a Meeting',
		};
		return reachOutMe[parameter];
	}
};
module.exports = Configuration; 
