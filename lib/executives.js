var base64Img = require('base64-img');

class Executives {
	getLeadershipMenu() {
		return [
			"Management Team",
			"Advisory Board"
		];
	} 
	 
	getManagementMenu() {
		return [
			"CEO / Founder",
			"CDO",
			"CTO",
			"Human Resources",
			"Enterprise Sales",
			"Strategy & Marketing",
		];
	}
	getResponse(parameter) {
		var responses = {
			"Leadership": "As part of our Leadership, we have an Advisory Board and a Management Team. \r\n\r\n Which Team would you like to know more about?",
			"Management": "Suyati has a highly qualified and experienced Senior Management Team. \r\n\r\n Whom would you like to know more about?",
			"Advisory": "Suyati has a set of strong, independent Experts guiding us at every step of our way. \r\n [ Know more... ](https://suyati.com/about-us/advisory-board/)",
			'Video':'Watch one of his talks here:',
		};
		return responses[parameter];
	}
	returnExecutiveCard(parameter) {
		switch (parameter) {
			case "CEO":
				return {
					title: "Mukund Krishna",
					subtitle: 'Founder, CEO',
					text: 'Digital Transformation Evangelist with 20+ years of experience in  Application Development,Project Execution and Mergers & Acquisitions.',
					img: base64Img.base64Sync('./images/management/' + parameter + '.png'),
					alt: 'Chief Executive Officer'
				};
				break;
			case "CDO":
				return {
					title: 'Praveen Parameswaran',
					subtitle: 'Chief Delivery Officer',
					text: 'Accomplished leader of Delivery Operations with over 20+ years of experience in Relationship & Change Management.',
					img: base64Img.base64Sync('./images/management/' + parameter + '.png'),
					alt: 'Chief Delivery Officer'
				};
				break;
			case "CPO":
				return {
					title: 'Vincent N P',
					subtitle: 'Director - Human Resources',
					text: 'A results driven HR growth creator with more than 20+ years of experience in \
    								organizational design, process mapping, soft skill training etc.',
					img: base64Img.base64Sync('./images/management/' + parameter + '.png'),
					alt: 'Director - Human Resources'
				};
				break;
			case "CTO":
				return {
					title: 'S Karthikeyan',
					subtitle: 'Chief Technology Officer',
					text: 'Thought leader in innovation with 19+ years of experience in Designing, \
    								Leading and Delivering world-class Software Solutions.',
					img: base64Img.base64Sync('./images/management/' + parameter + '.png'),
					alt: 'Chief Technology Officer'
				};
				break;
			case "CMO":
				return {
					title: 'Revathi Krishna',
					subtitle: 'Director — Strategy & Marketing',
					text: '20+ years of experience in IT and Consulting space specialising in Entrepreneurship and Communications Strategy.',
					img: base64Img.base64Sync('./images/management/' + parameter + '.png'),
					alt: 'Director—Strategy & Marketing'
				};
				break;
			case "ES":
				return {
					title: 'Ashutosh Karandikar',
					subtitle: 'Director - Enterprise Sales',
					text: '12+ years of global experience across ERP / CRM Consulting, IT Product / Services Sales and New Vertical/ Market Development.',
					img: base64Img.base64Sync('./images/management/' + parameter + '.png'),
					alt: 'Director of Enterprise Sales'
				};
		}
	}

}
module.exports = Executives;