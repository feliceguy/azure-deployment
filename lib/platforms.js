var base64Img = require('base64-img');

class Platforms {
	getResponse() {
		return "We have expertise in the following platforms. \r\n\r\n Which of them would you like to know more about?";
	}

	getPlatformsMenu() {
		return [
			"CMS",
			"CRM",
			"Ecommerce"
		];
	}
	getCmsMenu() {
		return [
			"EPI Server",
			"Ektron",
			"Sitecore",
			"Sitefinity",
			"Kentico",
			"Drupal",
			"Wordpress",
			"Joomla"
		];
	}
	getCrmMenu() {
		return [
			"Salesforce",
			"Microsoft Dynamics CRM",
			"SugarCRM Services"
		];
	}
	getEcommerceMenu() {
		return [
			"Magento",
			"NOPCommerce",
			"OpenCart",
			"Commerce Server",
		];
	}

	getPlatformResponse(parameter) {
		var response = {
			"CMS": "The Internet is about content and Suyati is about managing, displaying, and creating brilliant user experiences.\r\n\r\n Which type of CMS Service would you like to know more about?",
			"EPI Server": "Suyati has helped customers to empower their businesses with successful websites to portray their services and offerings using Episerver.",
			"Ektron": "Suyati is Ektron’s Premier Partner in India, adjudged as one among the best in APAC, and acknowledged by Ektron as one of the best solutions partner.",
			"Sitecore": "Suyati helps you get personalized and context-related service that will transform your business to \'digital-first\' using Sitecore.",
			"Sitefinity": "Sitefinity CMS ultimate aim to reduce the complexity in web development and Suyati's endeavour to reduce complexity in user experience creates a winner CMS.",
			"Kentico": "Suyati builds, customizes and manages Kentico sites.",
			"Drupal": "Drupal is an enthusiastic CMS and Suyati is even more zealous to build amazing sites for its clients in Drupal.",
			"Wordpress": "Wordpress is used by top 10 million websites of the world, and adjudged the most sought-after blogging platform by around 60 million websites.",
			"Joomla": "Suyati’s expertise in Joomla helps in building and delivering easy-to-use, data driven, and information rich sites and applications.",
			"CRM": "Suyati helps in developing Microsoft Dynamics CRM solutions which enables clients to further their sales and business objectives. \r\n\r\n Which type of CRM Service would you like to know more about?",
			"Salesforce": "Suyati is a Salesforce Consulting Partner and an AppExchange Partner with 40+ years of cumulative experience.",
			"Microsoft Dynamics CRM": "Suyati's range of MS Dynamics services enable enterprises to better their customer expectations at every point of contact.",
			"SugarCRM Services": "SugarCRM is a visionary and innovator and is being used by 60,000+ users in around 120 nations.",
			"Ecommerce": "Ecommerce solutions are unique since everyone out there is a potential buyer. \r\n\r\n Which type of Ecommerce Service would you like to know more about?",
			"Magento": "Suyati has experience with Magento’s latest versions and can fulfil all the business needs of a client including orders, promotions etc.",
			"NOPCommerce": "Superior flexibility and control is what makes the nopCommerce solutions stand out.",
			"Commerce Server": "Suyati helps to create multi-channel commerce applications by leveraging the wide-ranging power of Commerce Server.",
			"OpenCart": "The beauty of Open Cart is that it’s an out-of-the-box e-commerce solution, which blends perfectly with Suyati as a company as we don’t stick to convention.",
		};
		return response[parameter];
	}
	getLinks(parameter) {
		var links = {
			"Cms": "https://suyati.com/platforms/cms/",
			"EPI Server": "https://suyati.com/platform/episerver-cms-solutions/",
			"Ektron": "https://suyati.com/platform/ektron-cms-solutions/",
			"Sitecore": "https://suyati.com/platform/sitecore-cms-solutions/",
			"Sitefinity": "https://suyati.com/platform/sitefinity-cms-solutions/",
			"Kentico": "https://suyati.com/platform/kentico-cms-solutions/",
			"Drupal": "https://suyati.com/platform/drupal-cms-solutions/",
			"Wordpress": "https://suyati.com/platform/wordpress-cms-solutions/",
			"Joomla": "https://suyati.com/platform/joomla-cms-development/",
			"Crm": "https://suyati.com/platforms/crm/",
			"Salesforce": "https://suyati.com/platform/salesforce-consulting/",
			"Microsoft Dynamics CRM": "https://suyati.com/platform/microsoft-dynamics-crm/",
			"SugarCRM Services": "https://suyati.com/platform/sugarcrm-solutions/",
			"Ecommerce": "https://suyati.com/platforms/ecommerce/",
			"Magento": "https://suyati.com/platform/magento-solutions/",
			"NOPCommerce": "https://suyati.com/platform/nopcommerce-solutions/",
			"Commerce Server": "https://suyati.com/platform/commerce-server-solutions/",
			"OpenCart": "https://suyati.com/platform/opencart-solutions/",
		};
		return links[parameter];
	}

	returnPlatformCard(parameter) {		
		return {
			title: parameter,
			subtitle: "Platform",
			img: base64Img.base64Sync('./images/platforms/' + parameter + '.svg'),
			text: Platforms.prototype.getPlatformResponse(parameter),
			url: Platforms.prototype.getLinks(parameter)
		};
	}
};
module.exports = Platforms;