class Careers {
	getResponse(parameter) {
		var responses = {
			"India": "Please click [here](https://suyati.com/careers/openings/) to know more about the job openings in India.",
				"USA": "Please click [here](https://suyati.com/careers/us-openings/) to know more about the job openings in USA.",
			"Jobs": "We have several open positions at present.\r\n\r\nIn which of our locations would you like to work at?",
			'Video' : 'Watch the below video to know more:',
		};
		return responses[parameter];
	}
	
	getLocation() {
		return ['India', 'USA'];
	}
}
module.exports = Careers;