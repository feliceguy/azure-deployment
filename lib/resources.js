class Resources {

	getResourceResponse() {
		return "Which type of resources would you like to access?";
	}
	getResponse() {
		return "We have a huge collection of case studies, white papers, webinars, videos etc. on various topics. \r\n\r\n Which of \
			    the below sections would you like to know more about?";
	}

	getResourceMenu() {
		return [
			"Platform",
			"Technology",
			"Industry"
		];
	}

	geSubResourceMenu() {
		return [
			"White Papers",
			"Case Study",
			"Webinars",
			"Blog"
		];
	}

	returnResourceLink(parameter) {
		var msg = "To know more or to download, please click ";
		var responses = {
			"Industry-White Papers": msg + "[here.](http://suyati.com/filtered-white-papers?type=industries)",
			"Technology-White Papers": msg + "[here.](http://suyati.com/filtered-white-papers?type=technologies)",
			"Platform-White Papers": msg + "[here.](http://suyati.com/filtered-white-papers?type=platform)",
			
			"Industry-Case Study": msg + "[here.](https://suyati.com/filtered-case-studies?type=industries)",
			"Technology-Case Study": msg + "[here.](https://suyati.com/filtered-case-studies?type=technologies)",
			"Platform-Case Study": msg + "[here.](https://suyati.com/filtered-case-studies?type=platform)",
			"Industry-Webinars": msg + "[here.](https://suyati.com/filtered-webinars?type=industries)",
			"Technology-Webinars": msg + "[here.](https://suyati.com/filtered-webinars?type=technologies)",
			"Platform-Webinars": msg + "[here.](https://suyati.com/filtered-webinars?type=platform)",
			"Technology-Blog": "To know more, please click [here.](Https://suyati.com/blogs/?cat=technology)",
			"Platform-Blog": "To know more, please click [here.](https://suyati.com/blogs/?cat=platforms)",
			"Industry-Blog": "To know more, please click [here.](https://suyati.com/blogs/?cat=industry)",

		};
		return responses[parameter];
	}
}
module.exports = Resources;