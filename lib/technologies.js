var base64Img = require('base64-img');
class Technologies {

	getResponse() {
		return ["We have expertise in the following type of Technologies. \r\n\r\n Which type would you like to know more about?"];
	}

	getTechnologiesMenu() {
		return [
			"Microsoft",
			"Open Source",
			"Mobile",
			"Cloud",
			"Big Data"
		];
	}

	getMicrosoftMenu() {
		return [
			".Net",
			"Biz Talk",
			"SQL Server",
			"SharePoint",
		];
	}

	getOpensourceMenu() {
		return [
			"Node.Js",
			"Php",
			"Python",
			"Ruby on Rails"
		];
	}

	getMobileMenu() {
		return [
			"Windows",
			"Android",
			"iOS"
		];
	}
	getCloudMenu() {
		return [
			"Force.com",
			"Heroke",
			"Azure",
			"Amazon Web Services"
		];
	}

	getBigdataMenu() {
		return [
			"Big Data Smack",
			"Couch DB",
			"Mongo DB"
		];
	}

	getTechnologyResponse(parameter) {
		var responses = {
			"Microsoft": "Suyati is a Microsoft Gold Certified Partner. \r\n\r\n Which type of Microsoft technology would you like to know more about?",
			".Net": "Suyati has developed two products and over 20 web applications on the ASP.NET MVC framework.",
			"Biz Talk": " Our customers trust Microsoft BizTalk as it offers wide ranging solutions from payment processing to real-time decision making.",
			"SQL Server": "Suyati has 30+ years’ cumulative experience in database management services, including remote databases.",
			"SharePoint": "Suyati develops customized SharePoint solutions, offer Windows SharePoint Services, and Microsoft Office SharePoint Server solutions.",
			"Open source": "A collaborative and open environment is an integral part of our corporate culture. \r\n\r\n Which Open source technology would you like to know more about?",
			"Node.Js": "At Suyati, we help enterprises succeed with the best of Node.",
			"Php": "Web Development Solutions with rich features illustrates Suyati’s capabilities in the powerful PHP framework",
			"Python": "Using Python’s open source, flexible, and dynamic language, Suyati possess the expertise in developing web applications.",
			"Ruby on Rails": "When it comes to time and money, Ruby on Rails is undoubtedly an excellent choice for enterprises for developing applications.",
			"Mobile": "\'Mobile-first, everything else follows\' is Suyati\'s dictum for mobile development. \r\n\r\n Which Mobile technology would you like to know more about?",
			"Windows": "Windows facilitates enterprise level management of mobile security and allows strong integration with Exchange Server.",
			"Android": "Suyati uses Android’s flexible open source licenses to design and distribute innovative apps using the Android and Java framework.",
			"iOS": "\"Unique design and innovative interfaces\” – this is Suyati\'s mantra for designing and developing rich iOS apps.",
			"Cloud": "Suyati helps companies migrate their existing applications onto the cloud as well as build cloud based applications from scratch. \r\n\r\n Which type of Cloud technology would you like to know more about?",
			"Force.Com": "With Force . com, Suyati excels in API Integration, Data Archival, com websites etc.",
			"Heroke": "Suyati\'s expertise in several programming languages makes Heroke a delight to work with.",
			"Azure": "Microsoft Azure, Microsoft’s public cloud computing platform provides a range of services, which includes those for analytics, storage and networking.",
			"Amazon Web Services": "Suyati collaborates with AWS to offer superior cloud services.",
			"Big Data": "Suyati derives meaningful insights from the humungous amounts of data being generated daily with our big data skills. \r\n\r\n Which Big Data service would you like to know more about?",
			"Big Data Smack": "Using Big Data Smack, Suyati can help you leverage real time information.",
			"Couch DB": "If your data is in a disconnected state, Suyati can build your go-to-database with CouchDB to achieve improved efficiency.",
			"Mongo DB": "MongoDB enables businesses across the globe to harness the power of data.",
		};
		return responses[parameter];
	}
	getTechnologyLink(parameter) {
		var links = {
			"Microsoft": "https://suyati.com/technologies/microsoft-technologies-solutions/",
			".Net": "https://suyati.com/technology/asp-net-solutions/",
			"Biz Talk": "https://suyati.com/technology/microsoft-biztalk-solutions/",
			"SQL Server": "https://suyati.com/technology/sql-server-solutions/",
			"SharePoint": "https://suyati.com/technology/sharepoint-solutions/",
			"Open source": "https://suyati.com/technologies/free-and-open-source-software-solutions/",
			"Node.Js": "https://suyati.com/technology/node-js-solutions/",
			"Php": "https://suyati.com/technology/php-solutions/",
			"Python": "https://suyati.com/technology/python-solutions/",
			"Ruby on Rails": "https://suyati.com/technology/ruby-on-rails-solutions/",
			"Mobile": "https://suyati.com/technologies/mobile-solutions/",
			"Windows": "https://suyati.com/technology/windows-solutions/",
			"Android": "https://suyati.com/technology/android-solutions/",
			"iOS": "https://suyati.com/technology/ios-solutions/",
			"Cloud": "https://suyati.com/technologies/cloud-solutions/",
			"Force.Com": "https://suyati.com/technology/force-com-solutions/",
			"Heroke": "https://suyati.com/technology/heroku-solutions/",
			"Azure": "https://suyati.com/technology/azure-solutions/",
			"Amazon Web Services": "https://suyati.com/technology/aws-amazon-web-services-solutions/",
			"Big Data": "https://suyati.com/technologies/big-data-consulting-solutions/",
			"Big Data Smack": "https://suyati.com/technology/big-data-smack/",
			"Couch DB": "https://suyati.com/technology/couch-db-solutions/",
			"Mongo DB": "https://suyati.com/technology/mongo-db-solutions/"
		};
		return links[parameter];
	}

	returnTechnologycard(parameter) {
		return {
			title: parameter,
			subtitle: "Technology",
			img: base64Img.base64Sync('./images/technologies/' + parameter + '.svg'),
			text: Technologies.prototype.getTechnologyResponse(parameter),
			url: Technologies.prototype.getTechnologyLink(parameter)
		};
	}
};

module.exports = Technologies;