var base64Img = require('base64-img');

class Industry {
	getResponse() {
		return ['We have expertise in the following industries. \r\n\r\n Which of our industry capability would you like to know more about?'];
	}
	getIndustriesMenu() {
		return [
			'Education',
			'Healthcare',
			'Start-Ups',
			'Publishing',
			'Manufacturing',
			'Retail',
			'Media & Entertainment',
		];
	}

	getIndustryResponse(parameter) {
		var response = {
			'Education': 'Suyati has experience leveraging technology to expand and improvise the learning experience in educational institutions.',
			'Publishing': 'Suyati is proficient in helping companies react proactively to their customer\'s switch from hard copies and desktops to handheld devices and eReaders.',
			'Media & Entertainment': 'Suyati ensures a seamless, thoughtful and successful transition from the legacy and traditional media practices for its clients.',
			'Manufacturing': 'Suyati offers customized solution for the industrial sector that opens doors to innovation and makes a direct impact on the top line.',
			'Retail': 'Suyati can help retailers provide incredible in-store experiences and offers several other business solutions that are transformational.',
			'Healthcare': 'Suyati has expertise in providing technical solutions and services for personalized healthcare.',
			'Start-Ups': 'Suyati helps by offering technological services that can help entrepreneurs settle down in business quickly and cost-efficiently.',
		};
		return response[parameter];
	}

	getLinks(parameter) {
		var links = {
			'Education': 'https://suyati.com/industries/education/',
			'Publishing': 'https://suyati.com/industries/publishing/',
			'Media & Entertainment': 'https://suyati.com/industries/media-entertainment/',
			'Manufacturing': 'https://suyati.com/industries/manufacturing/',
			'Retail': 'https://suyati.com/industries/retail-2/',
			'Healthcare': 'https://suyati.com/industries/healthcare-solutions/',
			'Start-Ups': 'https://suyati.com/industries/start-ups/',
		};
		return links[parameter];
	}

	returnIndustryCard(parameter) {
		return {
			title: parameter,
			subtitle: 'Industry',
			img: base64Img.base64Sync('./images/industry/' + parameter + '.svg'),
			text: Industry.prototype.getIndustryResponse(parameter),
			url: Industry.prototype.getLinks(parameter)
		};
	}
};

module.exports = Industry;