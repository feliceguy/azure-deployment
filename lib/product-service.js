var base64Img = require('base64-img');

class ProductService {
	getResponse() {
		return 'Which one would you like to know more about?';
	}
	
	getService() {
		return 'Suyati offers a wide range of services to it\'s Customers. \r\n\r\n Which service would you like to now more about?';
	}
	
	getMenu() {
		return [
			'Product',
			'Services'
		];
	}

	getServicesMenu() {
		return [
			'DX Consulting',
			'Robotic Process Automation',
			'Decision Science',
			'Net Promoter Score',
			'DX in 50 Days',
			'Digital Customer Experience',
			'Digital Quality Assurance'
		];
	}

	getServiceResponse(parameter) {
		var responses = {
			'Mekanate': 'Mekanate is a framework which helps Customers improve Operational Excellence, Customer Experience and thus digitally upgrade their DNA.',
			'DX Consulting': 'Suyati has a strong DX consulting service which helps Customers derive Digital Strategies for their Organizations.',
			'Decision Science': 'Suyati has a strong Decision Science team who can work on complex data sets and generate insights.',
			'DX in 50 Days': 'Suyati\'s \'DX in 50 days\' is meant for Customers who want quick Digital Transformations in Core Operations.',
			'Robotic Process Automation': 'Suyati can help automate a Customer\'s repetitive tasks like data entry, form filling, email extraction using RPA.',
			'Digital Customer Experience': 'Suyati\'s CX practice helps organizations improve their Customer Experience.',
			'Net Promoter Score': 'Suyati\'s Net Promoter framework helps Customers understand their User segments and align business strategies accordingly.',
			'Digital Quality Assurance': 'Suyati\'s Digital Quality Assurance Team has expertise in testing AI engines, Data Modelling, RPA and IoT solutions.',
		};
		return responses[parameter];
	}
	
	getServiceLink(parameter) {
		var links = {
			'Mekanate': 'https://suyati.com/mekanate/',
			'DX Consulting': 'https://suyati.com/mekanate/',
			'Decision Science': 'https://suyati.com/mekanate/',
			'DX in 50 Days': 'https://suyati.com/mekanate/',
			'Robotic Process Automation': 'https://suyati.com/mekanate/',
			'Digital Customer Experience': 'https://suyati.com/mekanate/',
			'Net Promoter Score': 'https://suyati.com/mekanate/',
			'Digital Quality Assurance': 'https://suyati.com/mekanate/',
		};
		return links[parameter];
	}

	returnServicesCard(parameter) {
		return {
			title: parameter,
			subtitle: 'Digital Transformation Service',
			img: base64Img.base64Sync('./images/product-service/Services.png'),
			text: ProductService.prototype.getServiceResponse(parameter),
			url: ProductService.prototype.getServiceLink(parameter)
		};
	}

	returnProductCard(parameter) {
		return {
			title: 'Mekanate',
			subtitle: 'Digital Transformation Framework',
			img: base64Img.base64Sync('./images/product-service/' + parameter + '.png'),
			text: ProductService.prototype.getServiceResponse('Mekanate'),
			url: ProductService.prototype.getServiceLink('Mekanate')
		};
	}
}
module.exports = ProductService;