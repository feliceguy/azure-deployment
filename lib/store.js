class Store {

	getMenu() {
		return [
			'Sales & Marketing',
			'Products & Services',
			'Leadership Team',
			'Industry',
			'Platform',
			'White Papers & Case Studies',
			'Technology',
			'Jobs',
		];
	}

	getRating() {
		return ['1', '2', '3', '4', '5'];
	}
	getCallbackResponse(parameter) {
		var response = {
			'Phone': 'Which phone number can we reach you?',
			'Email': 'Which email ID can we contact you?',
			'Name' : 'Your name please?',
			'Contact' : 'Please connect with us at: \r\n\r\n Email ID: services@suyati.com \r\n\r\n Phone Number: + (91) 484 424 2800', 
		};
				return response[parameter];
	}
	
	
	getResponse(parameter) {
		var response = {
			'Greeting': 'Hello, I am Mekbot, the Suyati Website Assistant. \r\n\r\n Pick a conversation bubble or type your question to get started. \r\n \r\n Note : Type \'Help\' at any point for assistance.',
			'Rate': 'On a scale of 1 to 5 (5 being the highest), how much would you rate my service?',
			'None' : 'I am so sorry, looks like I am not able to completely answer your questions. I will connect you to an Executive who can help you better.',
			'Reachout' : 'Please connect with us at:\r\n\r\n Email ID: services@suyati.com\r\nPhone Number: + (91) 484 424 2800',
			'Thank' : 'Thank you! Our Executive will reach out to you. \r\n\r\n Have a good day!',
			'Welcome': 'You are welcome!!',
			'GoodDay': 'Have a good day!',
			'End': 'Thank you!',
			'Jobs': 'We do have job openings currently. In which of our locations would you like to work at?',
			'Digital Transformation': 'Digital transformation is a foundational shift to digital, mainly in how an organization delivers value to its customers.',
			'Help': 'Which of the below topics would you like to know more about?',
			'Bye': 'Bye! Have a good day!',
			'About': 'Suyati is a US based digital transformation solutions company providing software solutions across industries.',
			'Ideal': 'Hi! Just a reminder - Please type \'help\' if you need my assistance.',
			'Else': 'Do you need any additional assistance?',
			'Business': 'Suyati is a Digital Transformation Solutions company helping businesses to implement impactful digital initiatives.',
			'Okay': 'Okay! A quick reminder, to find out what I can do for you, just type help.',
			'Video' : 'Watch the below video to know more:',
			'Partners' : 'Mircosoft, Episerver, Sitecore and Mongo DB are our partners. Please click [here]( https://suyati.com/about-us/partners/) to read more about the partnerships.',
			'Origin' : 'Suyati was started in 2009 in Kerala. View Suyati\'s growth story [here](https://suyati.com/about-us/our-story/)',
		};
		return response[parameter];
	}

	getRatingResponse(parameter) {
		var response = [
			'What went wrong? \r\n\r\n Why wasn\'t this a perfect experience?',
			'A good experience ! \r\n\r\n Why wasn\'t this a perfect experience?',
			'A great Experience ! What went perfect?',
		];
		return response[parameter];
	}

	getRatingList(parameter) {
		var response = {
			'lowRating': [
				'Bot misunderstood',
				'Bot not helpful',
				'Others',
			],
			'highRating': [
				'Helpful chatbot',
				'Easy to use chatbot',
				'Others'
			]
		};
		return response[parameter];
	}
	getOtherRatingResponse(parameter) {
		var response = [
			'Please tell us what went wrong',
			'Please tell us why this wasn\'t a perfect experience',
			'Please tell us what went perfect',
		];
		return response[parameter];
	}

	getBotResponse(parameter) {
		var response = {
			'Name': 'I am MekBot, the brainchild of the Suyati Innovation Team. \r\n \r\n How can I help you?',
			'State': 'I am very well, thank you for asking! \r\n \r\n How may I help you?',
			'Age': 'I have been active in service since Jan 2018!',
			'Appearance': 'I look even better than my Avatar.',
			'Hobbies': 'I would say that helping people is my hobby.',
			'Human': 'I am not a human being.',
			'Gender': 'I am just a bot, no gender :)',
			'Location': 'I live right here :)',
			'Locality': 'I am right here if you need me. \r\n\r\n Would you like to know about any of the below topics?',
			'Language': 'Sorry! Currently I speak English only.',
			'Thanks': 'You are welcome!',
			'Bye': 'Bye! Have a good day!',
			'Thank': 'Thank you!',
			'Smiley': ':)',
			'Skills': 'I can help you in the following topics',
		};
		return response[parameter];
	}

	getNoneResponse() {
		var response = [
		'Sorry, I am not able to figure out what you are trying to say. Please rephrase your question.\r\n\r\n Alternatively, type \'help\' to understand what I can help you with.',
		'Unfortunately, I didn\'t understand what you are trying to say. Kindly rephrase your question.\r\n\r\n A quick reminder, to find out what I can do for you, just type help.',
		'I am sorry, I am not able to recognize your question. Would you please rephrase.\r\n\r\n Alternatively, type \'help\' to understand what I can assist you with.',
		'Kindly rephrase your question, I just wanted to make sure that I understand you correctly. \r\n\r\n Alternatively, type \'help\' to understand what I can assist you with.',
		'My Apologies, I didn\'t understand. \r\n\r\n A quick reminder, to find out what I can do for you, just type help.',
		'Apologies, I didn\'t really get you.\r\n\r\nJust type \'help\ to find out what I can help you with.',
		'Hmmm, Sorry I don\'t quite understand.Try typing \'help\' to find out what I can assist you with.',
		'I\'m sorry, I didn\'t get you. \r\n\r\n Type \'help\' to understand what I can assist you with.',
		'Unfortunately, I don\'t understand that. Can you try typing it in a different way? \r\n\r\n Try typing \'help\' to find out what I can assist you with.',	
			
		];
		return response;
	}
}
module.exports = Store;
