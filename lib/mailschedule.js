var Configuration = require('../config')
var sendmail = require('sendmail')(silent = false);
var icalToolkit = require('ical-toolkit');
var builder = null;

class Mailing {

	scheduleMeeting(parameters) {
		builder = icalToolkit.createIcsFileBuilder();
		var time = parameters.Time;
		var datetime = parameters.Date + " " + time;
		var sDate = new Date(datetime);
		var eDate = new Date(datetime);
		sDate.setHours(sDate.getHours() - 5);
		sDate.setMinutes(sDate.getMinutes() - 30);
		eDate.setHours(eDate.getHours() - 4);
		eDate.setMinutes(eDate.getMinutes() - 30);
		
		builder.spacers = true;
		builder.NEWLINE_CHAR = '\r\n';
		builder.throwError = false;
		builder.ignoreTZIDMismatch = true;	
		// time zone
		builder.timezone = Configuration.prototype.getMailDetails('Timezone'),  
		builder.tzid =  Configuration.prototype.getMailDetails('Timezone'),
		builder.method = 'REQUEST';
		// Add events
		builder.message =
		builder.events.push({
			start: sDate,
			end: eDate,
			transp: 'OPAQUE',
			summary: 'Meeting with Suyati',
			alarms: [15, 10, 5],
			uid: null,
			sequence: null,
			stamp: new Date(),
			floating: false,
			location: Configuration.prototype.getMailDetails('Location')  ,
			description:  Configuration.prototype.getMailDetails('Description')  ,
			// Organizer info
			organizer: {
				name: Configuration.prototype.getMailDetails('OrganizerName') ,
				email: Configuration.prototype.getMailDetails('Organizeremail') 
			},
			method: 'PUBLISH',
			status: 'CONFIRMED',
		});

		var message = "Hello " + parameters.Name + ",<br><br>Meeting has been set up with our Sales Team. Kindly accept/reject the meeting invite \
	                to acknowledge.<br><br>Thanks & Regards,<br>Suyati Sales & Marketing Team";
		var icsFileContent = builder.toString();
		if (icsFileContent instanceof Error) {
			console.log('Returned Error, you can also configure to throw errors!');
		}
		sendmail({
			from: Configuration.prototype.getMailDetails('From') ,
			to: parameters.Email + ", " + Configuration.prototype.getMailDetails('To') ,
			subject:  Configuration.prototype.getMailDetails('Subject') ,
			html: message,
			alternatives: [{
				contentType: 'text/calendar; charset="utf-8"; method=REQUEST',
				content: icsFileContent.toString()
			}],
		}, function (err, reply) { });
	}
		
	sendUserMail(schedule,message,salesResponse)
	{
			sendmail({
			from:  Configuration.prototype.getMailDetails('From')  ,
			to: schedule.Email ,
			subject:  Configuration.prototype.getMailDetails('ScheduleSubject') ,
			html:message
		}, 
		function(err, reply) {
			if (salesResponse=='Contact Sales Team') {
				message =  Configuration.prototype.getContactSalesTeam('Body')  + ' Name: ' + schedule.Name + '<br>Email ID: ' + schedule.Email + '<br>Contact Number: ' + schedule.Phone +
        				 '<br>Purpose: ' + salesResponse + '<br><br> ' + Configuration.prototype.getContactSalesTeam('Signature')   ;
			} else {
				message = Configuration.prototype.getReachOutMe('Body')  + ' Name: ' + schedule.Name + '<br>Email ID: ' + schedule.Email + '<br>Contact Number: ' + schedule.Phone +
         				  '<br>Purpose: ' + salesResponse + '<br><br> ' + Configuration.prototype.getContactSalesTeam('Signature') ;
			}
			sendmail({
			from: Configuration.prototype.getMailDetails('From') ,
			to: Configuration.prototype.getMailDetails('To') ,
			subject:  Configuration.prototype.getContactSalesTeam('Subject') ,
			html:message
		}, function (err, reply) { });
		});
	}
	sendCallbackMail(schedule,salesResponse )
	{	
		var message =  Configuration.prototype.getContactSalesTeam('Body')  + ' Name: ' + schedule.Name + '<br>Email ID: ' + schedule.Email + '<br>Contact Number: ' + schedule.Phone +
        				 '<br><br> ' + Configuration.prototype.getContactSalesTeam('Signature')   ;
								sendmail({
			from: Configuration.prototype.getMailDetails('From') ,
			to: Configuration.prototype.getMailDetails('To') ,
			subject:  Configuration.prototype.getContactSalesTeam('Subject') ,
			html:message
		}, function (err, reply) { });
	}
	
}
module.exports = Mailing;