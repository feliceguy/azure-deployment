var base64Img = require('base64-img');
class SalesMarketing {
	getResponse() {
		return "Which would you like to know more about?";
	}

	getMenu() {
		return [
			"Schedule a Meeting",
			"Contact Sales Team"
		];
	}

	getScheduleMenu() {
		return [
			"Schedule my own meeting",
			"Reach out to me"
		];
	}

	getResponse(parameter) {
		var responses = {
			"Date": "Which date would you like to schedule the meeting? \r\n\r\n Please type a future date in one of the below formats: \r\n mm/dd/yyyy \r\n dd month yyyy \r\n mm-dd-yyyy",
			"Topic": "What topics would you like to be covered in the meeting?",
			"Name": "Your name please?",
			"Time": "What time would you like to schedule the meeting ? \r\n\r\n Please type the time in the 12-hour format [HH:MM pm/am].",
			"Email": "Which email ID can we contact you?",
			"Phone": "Which phone number can we reach you?",
			"Thank": "Thank you! \r\n\r\n Our Sales Team will reach out to you to set up a meeting. Have a good day!",
			"Thanks": "Thank you! Our Sales Team will reach out to you. \r\n\r\n Have a good day!",
			"Schedule": "Would you prefer to set up your own meeting or have us reach out to you?",
			"NoResponse": "Please connect with us at:\r\nEmail ID: services@suyati.com \r\n Phone Number: + (91) 484 424 2800",
			"InvalidDate": "This date seems to be invalid. \r\n\r\n Please type a future date in one of the below formats:\r\n mm/dd/yyyy \r\n dd month yyyy \r\n mm-dd-yyyy",
			"InvalidTime": "This time seems to be invalid. \r\n\r\n Please retype the time in the 12-hour format [HH:MM pm/am].",
			"InvalidEmail": "This ID seems to be invalid. Would you please provide your valid Email ID?",
			"InvalidPhone": "This number seems to be incorrect. Would you please provide your valid phone number?",
		};
		return responses[parameter];
	}
}
module.exports = SalesMarketing;